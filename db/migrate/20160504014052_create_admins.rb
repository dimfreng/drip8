class CreateAdmins < ActiveRecord::Migration
  def up
    create_table :admins do |t|
      t.column :username, :string
      t.column :password_digest, :text
      t.timestamps null: false
    end
  end

  def down
  	drop_table :admins
  end
end
