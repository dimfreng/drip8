class CreateReports < ActiveRecord::Migration
  def up
    create_table :reports do |t|
      t.text :body
      t.references :drip, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def down
  	drop_table :reports
  end
end
