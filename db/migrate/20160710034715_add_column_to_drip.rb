class AddColumnToDrip < ActiveRecord::Migration
  def up
  	add_column :drips , :admin_id, :integer
  	add_index :drips, :admin_id
  	add_column :drips, :postasname, :text ,default: "none"
  	add_column :drips, :postastitle, :text ,default: "none"
  	add_column :users, :usertitle, :text
  	add_column :users, :number, :text
  	add_column :users, :nationality, :text
  	add_column :users, :reminder, :boolean, default: true
  end

  def down
  	remove_column :users, :usertitle
  	remove_column :users, :reminder
  	remove_column :users, :number
  	remove_column :users, :nationality
  	remove_column :drips, :postasname
  	remove_column :drips, :postastitle
  	remove_index :drips , :admin_id
  	remove_column :drips, :admin_id
  end
end
