class AddColumn < ActiveRecord::Migration
  def up
  	rename_column :features, :description, :author
  end

  def down
  	rename_column :features, :author, :description
  end
end

