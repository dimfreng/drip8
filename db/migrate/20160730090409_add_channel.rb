class AddChannel < ActiveRecord::Migration
  def up
  	add_column :drips ,:channel, :text
  end

  def down
  	remove_column :drips ,:channel
  end
end
