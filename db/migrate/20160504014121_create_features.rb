class CreateFeatures < ActiveRecord::Migration
  def up
    create_table :features do |t|
      t.column :link, :text
      t.column :quote, :text
      t.column :type, :string
      t.column :description, :text
      t.column :status, :string
      t.timestamps null: false
    end
  end

  def down
  	drop_table :features
  end
end
