class CreateSocialmedia < ActiveRecord::Migration
  def change
    create_table :socialmedia do |t|
      t.column :user_id, :integer
      t.column :link, :text
      t.index  :user_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :socialmedia
  end
end
