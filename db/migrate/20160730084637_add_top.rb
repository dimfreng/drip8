class AddTop < ActiveRecord::Migration
  def up
  	add_column :drips , :top, :boolean ,:default => false
  end

  def down
  	remove_column :drips, :top
  end
end
