class CreateBuckets < ActiveRecord::Migration
  def up
    create_table :buckets do |t|
      t.column :name, :text
      t.column :status, :string
      t.column :user_id, :integer
      t.index  :user_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :buckets
  end
end
