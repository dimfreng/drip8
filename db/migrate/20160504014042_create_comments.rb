class CreateComments < ActiveRecord::Migration
  def up
    create_table :comments do |t|
      t.column :content, :text
      t.column :user_id, :integer
      t.column :drip_id, :integer
      t.index  :user_id
      t.index  :drip_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :comments
  end
end
