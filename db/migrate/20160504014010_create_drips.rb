class CreateDrips < ActiveRecord::Migration
  def up
    create_table :drips do |t|
      t.column :title, :text
      t.column :description, :text
      t.column :link, :text
      t.column :status, :string
      t.column :user_id, :integer
      t.column :bucket_id, :integer
      t.column :shares, :integer
      t.index  :bucket_id
      t.index  :user_id
      t.timestamps null: false
    end
  end


  def down
  	drop_table :drips
  end
end
