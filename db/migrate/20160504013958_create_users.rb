class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.column :firstname, :text
      t.column :lastname, :text
      t.column :email, :text
      t.column :profile_picture, :text
      t.column :state, :string
      t.column :password_digest, :text
      t.timestamps null: false
    end
  end

  def down
  	drop_table :users
  end

end
