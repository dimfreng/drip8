class User < ActiveRecord::Base
	has_secure_password
	has_many :socialmedia
	has_many :buckets, -> { includes :drips }, dependent: :destroy
	has_many :drips,-> { includes :comments }, dependent: :destroy
	has_many :comments,-> { includes :user }, dependent: :destroy
	has_many :votes, ->{ includes :drip }, dependent: :destroy

	def self.get_user (id)
		return self.find_by_id(id).as_json(except: :password_digest, include: :buckets, include: :socialmedia)
	end

	def self.get_all_user 
		return self.all.as_json(except: :password_digest, include: :buckets, include: :socialmedia)
	end


end
