class Bucket < ActiveRecord::Base
	belongs_to :user
	has_many :drips,-> { includes :comments }, dependent: :destroy


	def self.get_bucket (id)
		return self.find_by_id(id).as_json(include: [{user: {except: :password_digest}} , :drips])
	end

	def self.get_all_bucket
		return self.all.as_json(include: [{user: {except: :password_digest}} , :drips])
	end

	def self.get_buckets_by_user ( user_id )
		return self.where("user_id = ?", user_id).as_json(include: [{user: {except: :password_digest}} , :drips])
	end

end
