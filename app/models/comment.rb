class Comment < ActiveRecord::Base
	belongs_to :user
	belongs_to :drip

	def self.get_comment (id)
		return self.find_by_id(id).as_json(include: [{user: {except: :password_digest}}, :drip])
	end

	def self.get_comments_by_user ( user_id )
		return self.where("user_id = ?", user_id).as_json(include: [{user: {except: :password_digest}}, :drip])
	end

	def self.get_all_comment_by_drip ( drip_id )
		return self.where("drip_id = ?", drip_id).as_json(include: [{user: {except: :password_digest}}, :drip])
	end
end
