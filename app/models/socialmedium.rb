class Socialmedium < ActiveRecord::Base
	belongs_to :users

	def self.get_media_by_user_id(id)
		return self.where("user_id = ? ", id).order('id DESC').as_json()
	end
end
