class Vote < ActiveRecord::Base
	belongs_to :drip
	belongs_to :user

	def self.get_all_vote_by_drip ( drip_id )
		return self.where("drip_id = ?", drip_id).as_json(include: [{user: {except: :password_digest}}])
	end

	def self.get_votes_by_user ( user_id )
		return self.where("user_id = ?", user_id).as_json(include: [:drip])
	end

end
