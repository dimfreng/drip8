class Report < ActiveRecord::Base
  belongs_to :drip

  def self.get_all_reports
  	return self.all.as_json(include: [:drip])
  end
end
