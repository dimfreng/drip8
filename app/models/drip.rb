class Drip < ActiveRecord::Base
	belongs_to :user
	belongs_to :bucket
	belongs_to :admin
	has_many :reports
	has_many :comments,-> { includes :user }, dependent: :destroy
	has_many :votes,-> { includes :user }, dependent: :destroy


	def self.get_drip (id)
		return self.find_by_id(id).as_json(include: [{user: {except: :password_digest}} , {bucket: {include: :drips}}, {comments: {include: [{ user: {except: :password_digest} }] }}, :votes])
	end

	def self.get_all_drip_of_users
		return self.where("admin_id IS ? and user_id IS NOT NULL", nil).order('id DESC').as_json(include: [{user: {except: :password_digest}} , {bucket: {include: :drips}}, :comments, :votes])
	end

	def self.get_all_drip_of_admin
		return self.where("user_id IS ?", nil).order('id DESC').as_json(include: [{admin: {except: :password_digest}} , {bucket: {include: :drips}}, :comments, :votes])
	end

	def self.get_all_drip_of_anon
		return self.where("user_id IS ? and admin_id IS ?", nil , nil).order('id DESC').as_json(include: [:comments, :votes])
	end

	def self.get_all_top_drips
		return self.where("top = ?", false).order('updated_at DESC').as_json(include: [{admin: {except: :password_digest}} ,{user: {except: :password_digest}} , {bucket: {include: :drips}}, :comments, :votes])
	end

	def self.get_drips_by_user ( user_id )
		return self.where("user_id = ?", user_id).as_json(include: [{user: {except: :password_digest}} , {bucket: {include: :drips}}, :comments, :votes])
	end

end
