class Admin < ActiveRecord::Base
	has_secure_password
	has_many :drips,-> { includes :comments }, dependent: :destroy
end
