drip
	.factory( "Server" , [
		"$http",
		function factory ( $http ) {

			var server = {
				"request": function ( method , path , params ) {
					return $http[ method ]( path , ( params || { } ) );
				},
				"setSession": function ( userData ) {
					return server.request( "post" , "/session/set" , { "user": userData } );
				},
				"getSession": function ( ) {
					return server.request( "post" , "/session/get" );
				},
				"authenticate": function ( email , password ) {
					return server.request( "post" , "/api/user/authenticate" , {
						"email": email,
						"password": password
					} );
				},
				"getSessionAdmin": function ( ) {
					return server.request( "get" , "/admin/session" );
				},
				"adminAuthenticate": function ( username , password ) {
					return server.request( "post" , "/api/admin/authenticate" , {
						"username": username,
						"password": password
					} );
				},
				"asset": function ( src ) {
					return server.request( "post" , "/asset/img" , { "src": src } );
				},
				"createUser": function ( userData ) {
					return server.request( "post" , "/api/new/user" , { "user": userData } );
				},
				"getAllDrips": function ( ) {
					return server.request( "get" , "/api/get/all/drip" );
				},
				"newBucket": function ( data ) {
					return server.request( "post" , "/api/new/bucket" , data );
				},
				"getBucketByUser": function ( id ) {
					return server.request( "post" , "/api/get/user/bucket" , { "user_id": id } );
				},
				"updateUser": function ( data ) {
					return server.request( "post" , "/api/user/update" , data );
				},
				"getUserBucket": function ( data ){
					return server.request( "post" , "/api/get/user/bucket" , data );
				},
				"addToTopDrip": function ( data ){
					return server.request( "post" , "/api/update/drip/top" , data );
				},
				"dropFromTopDrip": function ( data ){
					return server.request( "post" , "/api/update/drip/top" , data );
				},
				"getWhat": function ( method ){
					return server.request( "get" , '/api/get/' + method );
 				},
 				"getTopDrips": function (){
 					return server.request( "get" , '/api/get/top' );
 				},
 				"getDrip": function ( data ){
 					return server.request( "post" , '/api/get/drip' , data );
 				},
 				"newComment": function ( data ){
 					return server.request( "post" , '/api/new/comment' , data );
 				},
 				"newDrip": function ( data ){
 					return server.request( "post" , '/api/new/drip' , data );
 				},
 				"addQuote": function ( data ){
 					return server.request( "post" , '/api/new/feature' , data );
 				},
 				"addDripAdmin": function ( data ){
 					return server.request( "post" , '/api/new/drip' , data );
 				},
 				"updateChannel": function ( data ){
 					return server.request( "post" , '/api/update/drip/channel' , data );
 				},
 				"getSocial": function ( data ){
 					return server.request( "post" , '/api/get/social' , data );
 				},
 				"deleteDrip": function ( data ){
 					return server.request( "post" , '/api/update/drip' , data );
 				},
 				"updateSocial": function ( data ){
 					return server.request( "post" , '/api/update/social' , data );
 				},
 				"updateDrip": function ( data ){
 					return server.request( "post" , '/api/update/drip' , data );
 				}
			};

			return server;

		}
	] );
drip
	.factory( "quotesFactory" , [
		"$http",
		function factory ( $http ) {

			var quotes = [{
				"link": "https://www.youtube.com/watch?v=Nl5No2gnOZ0",
				"quote": "Success is simple. Do what's right, the right way, at the right time",
				"author": "Arnold H. Glasow"
			},{
				"link": "https://www.youtube.com/watch?v=lHnTeyMAf80",
				"quote": "Every great dream begins with a dreamer. Always remember, you have within you the strength, the patience, and the passion to reach to the stars ti change the world",
				"author": "Harriet Tubman"
			},{
				"link": "https://www.youtube.com/watch?v=TKn2kY3X5Sk",
				"quote": "The starting point of all achievement is desire",
				"author": "Napoleon Hill"
			},{
				"link": "https://www.youtube.com/watch?v=zLYECIjmnQs",
				"quote": "Success is not final, failure is not fatal: it is the courage to continue that counts",
				"author": "Winston Churchill"
			},{
				"link": "https://www.youtube.com/watch?v=AvXBBgG_OSk",
				"quote": "The way to get started is to quit talking and begin doing",
				"author": "Walt Disney"
			}];

			return quotes;

		}
	] );