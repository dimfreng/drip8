drip
	.factory( "Util" , [
		"$sce",
		function factory ( $sce ) {

			var utilities = {
				"closeModal": function ( ) {
					$( ".modal" ).modal( "hide" );
				},
				"trustUrl": function ( url ) {
					return $sce.trustAsResourceUrl( url );
				}				
			};

			return utilities;

		}
	] );