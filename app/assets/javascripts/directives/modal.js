drip
	.directive( "modal" , [
		'$rootScope',
		'Util',
		'postData',
		'getData',
		'Server',
		function directive ( $rootScope , Util , postData , getData , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {


					//console.log( "loaded modal" );
					function getSession(){
				    	Server.getSession(  ).then( function( data, status ){
				    		if( data.data != null ){
				    			scope.profileData = data.data.user;
								
				    		}
								
						} )
				    }
				    
				    function setVideoAndThumb(){
				    	scope.thumbnail = [];
					    scope.videoUrl = function ( url ) {
							var embeddedVideoUrl = "https://www.youtube.com/embed/" + url.split( "v=" )[1] + "?enablejsapi=1";
							return Util.trustUrl( embeddedVideoUrl );
						};
						scope.thumbUrl = function ( url , index ) {
							if( index != null ){
								var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
								scope.thumbnail[ index ] = embeddedVideoUrl;
								return scope.thumbnail[ index ] ;
							}else{
								var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
								return embeddedVideoUrl;
							}
							
						};
				    	
				    }

				    function getDrip( id  ){
				    	var data = {"id": id};
				    	Server.getDrip( data ).then( function( data, status ){
								scope.dripDetails = data.data.drip;
								setVideoAndThumb();
								console.log( data )
							} )
				    }
		            
		            getSession();
					scope.$on( 'send-drip-to-modal', function( evt, data ){
						scope.dripDetails = data;
						setVideoAndThumb();
						getDrip( data.id );

						
					} )
					scope.changeVideo = function( drip ){
						$rootScope.$broadcast( 'send-drip-to-modal', drip )
					}
					scope.addComment= function(){
						var newComment = {
								"comment":
								{
									"content": scope.comment, 
									"user_id": ( scope.profileData != null ) ? scope.profileData.id : null,
									"drip_id": scope.dripDetails.id
								}
							};
						Server.newComment( newComment ).then( function( data, status ){
								scope.comment = "";
								postData.postdata( '/api/get/drip', 
								{
									"id": scope.dripDetails.id
								}).success( function( data, status ){
									scope.dripDetails = data.drip;
									
								} )
							} )
					};
					scope.spitName = function spitName( user ){
						var name = "";
						if( user != null ){
							if( user.firstname != null ){
								
								name = user.firstname + " " + user.lastname;
								return name;
							}else{
								return false;
							}
						}
					};

					scope.spitTime = function spitTime( date ){
						var dateTime = new Date( date );
						var timediff = timeSince( dateTime );
						return timediff;
					};


					function timeSince(date) {
						//console.log( typeof date );
					    var seconds = Math.floor((new Date() - date) / 1000);

					    var interval = Math.floor(seconds / 31536000);

					    if (interval > 1) {
					        return interval + "y";
					    }
					    interval = Math.floor(seconds / 2592000);
					    if (interval > 1) {
					        return interval + "mon";
					    }
					    interval = Math.floor(seconds / 86400);
					    if (interval > 1) {
					        return interval + "day";
					    }
					    interval = Math.floor(seconds / 172800);
					    if (interval > 1) {
					        return interval + "days";
					    }
					    interval = Math.floor(seconds / 3600);
					    if (interval > 1) {
					        return interval + "hr";
					    }
					    interval = Math.floor(seconds / 60);
					    if (interval > 1) {
					        return interval + "min";
					    }
					    //console.log( interval );
					    //console.log( seconds );
					    return Math.floor(seconds) + " seconds";
					}
				}
			}
		}
	] );
