drip
	.directive( "dripsMainDirective" , [
		'Util',
		'$window',
		'$rootScope',
		'Server',
		function directive ( Util , $window , $rootScope , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"template": "<div ng-include='templateUrl'></div>",
				"link": function onLink ( scope , element , attributeSet ) {

					$window.onresize = function() {
		                changeTemplate();
		                scope.$apply();
		            };
		            changeTemplate();
		            $rootScope.$on( 'get-top-drips', function( evt, data ){
		            	getTopDrips()
		            } )

		            scope.name = "January August";
		            function changeTemplate() {

		                var screenWidth = $window.innerWidth;
		                if (screenWidth <= 768) {
		                    scope.templateUrl = 'sml-dev.html';
		                } else if (screenWidth > 768) {
		                    scope.templateUrl = 'dev-feed.html';
		                }
		            }
					scope.videoUrl = function ( url ) {						
						var embeddedVideoUrl = "https://www.youtube.com/embed/" + url.split( "v=" )[1] + "?enablejsapi=1";
						return Util.trustUrl( embeddedVideoUrl );
					};
					scope.thumbUrl = function ( url ) {						
							var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
							//console.log( embeddedVideoUrl )
							return embeddedVideoUrl;
						};
					getTopDrips()
					function getTopDrips(){
						Server.getTopDrips( ).then( function( data, status ){
							scope.dripsArray = data.data.drips;
						} )
					}
					
					
					scope.fbShare = function fbShare( drips ){
						var shares = drips.shares + 1;
						FB.ui({
						  method: 'share',
						  href: drips.link,
						  caption: "www.drip8.com",
						}, function(response){});
						Server.updateDrip( {"id":drips.id,"drip":{"shares": shares}} ).success( function( data ){
							console.log( data );
						} )
					};

					scope.spitFlag = function spitFlag( user ){
						var flag = "";
						
						if( user != null ){
							if( user.nationality != null ){
								if( user.nationality.split( "&&" ).length == 2 ){
									scope.flag = "images/flags-mini/" + user.nationality.split( "&&" )[0].toLowerCase() + ".png"; 
									return true;
								}else{
									return false;
								}
							}else{
								return false;
							}
						}	
					};
					scope.spitName = function spitName( user ){
						var name = "";
						if( user != null ){
							if( user.firstname != null ){
								
								name = user.firstname + " " + user.lastname;
								return name;
							}else{
								return false;
							}
						}
					};
					scope.spitToolTip = function spitToolTip( user ){
						var name = "";
						if( user != null ){
							if( user.nationality != null ){
								
								var flag = user.nationality.split("&&");
								return flag[1];
							}else{
								return false;
							}
						}
					};
					scope.spitTitle = function spitTitle( user ){
						var title = "";
						if( user != null ){
							if( user.usertitle != null ){
								
								title = user.usertitle.split( "&&" );
								return title.join( ", " );
							}else{
							}
						}
					};

					scope.openModal = function openModal( drip ){
						
						$rootScope.$broadcast( 'send-drip-to-modal', drip )
					};
					
				}
			}
		}
	] );