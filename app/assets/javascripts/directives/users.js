drip
	.directive( "usersDirective" , [
		'getData',
		'Server',
		function directive ( getData , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {

					getData.getdata( '/api/get/all/user' ).success( function( data, status ){
						
						console.log( data );
						scope.users = data.users;
					} )
					//console.log( "users directive" )
					getSession();
					function getSession(){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.admin ){
				    		}else{
				    			window.location = "/logout";
				    		}
								
						} )
					}
				}
			}
		}
	] );