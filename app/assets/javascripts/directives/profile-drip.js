drip
	.directive( "profileDrip" , [
		"Server",
		"Util",
		function directive ( Server , Util ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.user = { };
					scope.buckets = [ ];

					scope.getUserId = function ( ) {
						Server.getSession( )
							.then( function ( response ) {
								scope.user = response.data.user;
								scope.getBuckets( );
							} );
					};

					scope.getBuckets = function ( ) {
						Server.getBucketByUser( scope.user.id )
							.then( function ( response ) {
								scope.buckets = response.data.bucket;								
							} );
					};

					scope.thumbnail = function ( url ) {
						var thumbnailUrl = "http://img.youtube.com/vi/" + url.split("v=")[1] + "/0.jpg";
						return Util.trustUrl( thumbnailUrl );
					};

					scope.getUserId( );
				}
			}
		}
	] );