drip
	.directive( "socialMedia" , [
		'Server',
		function directive ( Server ) {
			return {
				"restrict": "A",
				"scope": {
					"dripid": "="
				},
				"template": "<ul class='follow-me-links list-unstyled' ng-if='checkSocial()'>" +
					            "<li class='follow-label'>Follow me:</li>" +
					            "<li ng-if='dripSocial[0].link'><a target='_blank'href='{{dripSocial[0].link}}'><img src='/images/facebook-official.png' class='fa media-links' style='max-width:33px;max-height:19px;'></a></li>" +
					            "<li ng-if='dripSocial[1].link'><a target='_blank'href='{{dripSocial[1].link}}'><img src='/images/twitter.png' class='fa media-links' style='max-width:33px;max-height:19px;'></i></a></li>" +
					            "<li ng-if='dripSocial[2].link'><a target='_blank'href='{{dripSocial[2].link}}'><img src='/images/instagram.png' class='fa media-links' style='max-width:33px;max-height:19px;'></i></a></li>" +
					            "<li ng-if='dripSocial[3].link'><a target='_blank'href='{{dripSocial[3].link}}'><img src='/images/youtube.png' class='fa media-links' style='max-width:33px;max-height:19px;'></i></a></li>" +
					          "</ul>",
				"link": function onLink ( scope , element , attributeSet ) {
					Server.getSocial( { "id": scope.dripid } ).then( function( data, status ){
							scope.dripSocial = data.data.social;

							
							scope.checkSocial = function checkSocial(){
								if( scope.dripSocial[0].link != "" || scope.dripSocial[1].link != "" ||scope.dripSocial[2].link != "" ||scope.dripSocial[3].link != "" ){
									
									return true;
								}else{
								}
							};
						} )


				}
			}
		}
	] );