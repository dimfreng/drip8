drip
	.directive( "loadImageFirst" , [
		function directive ( ) {
			return {
				"restrict": "A",
				"scope": true,
				"compile": function compile( element , attributeSet ){
		            return{
		                pre: function pre ( scope , element , attributeSet ) {

							//console.log( "load image first loaded" );
							
						},
						post: function post( scope, element, attributeSet ){

							scope.loadFrame = function loadFrame( index , link ){
								scope.IFrame = [];
								scope.IFrame[ index ] = true;
								videoViews( link )


							};

							function videoViews( link ) {
							    var rex = /[a-zA-Z0-9\-\_]{11}/,
							        videoUrl = link === '' ? alert('Enter a valid Url'):link,
							        videoId = videoUrl.match(rex),
							        jsonUrl = 'https://gdata.youtube.com/feeds/api/videos/' + videoId + '?v=2',
							       embedUrl = '//www.youtube.com/embed/' + videoId,
							       embedCode = '<iframe width="350" height="197" src="' + embedUrl + '" frameborder="0" allowfullscreen></iframe>'
							    
							    console.log( videoId );
							    //console.log( videoUrl );
							    //console.log( jsonUrl );
							    //Get Views from JSON
							    $.getJSON(jsonUrl, function (videoData) {
							        var videoJson = JSON.stringify(videoData),
							            vidJson = JSON.parse(videoJson),
							            views = vidJson.entry.yt$statistics.viewCount;
							        //console.log( views );
							        console.log( videoData )
							    });
							    
							    //Embed Video
							    //$('.videoembed').html(embedCode);
							}
						}

		            }
		          }
				
			}
		}
	] );
