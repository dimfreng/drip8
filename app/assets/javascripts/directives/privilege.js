drip
	.directive( "privilegeDirective" , [
		'postData',
		'Server',
		function directive ( postData , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {

					scope.bucketForm 	= {};
					scope.quoteForm 	= {};
					scope.dripForm 		= {};

					getBucket();
					getSession();
					
					function getBucket(){
						Server.getBucketByUser( 1 ).then( function( data, status ){
							scope.dataBuckets = data.data.bucket;
						} )
					}
					function getSession(){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.admin ){
				    			scope.admin = data.data.admin;
				    		}else{
				    			window.location = "/drip8-adminshackle/logout";
				    		}
								
						} )
					}
					function updateChannel( id , channel ){
						var channel = {"id":id,"channel":channel};
						Server.updateChannel( channel ).then( function( data, status ){
							
						} )
					}
					scope.addBucket = function(){
						var newBucket = {
								"bucket":
								{
									"name": scope.bucketForm.bucketName,
									"status":"ok",
									"user_id": 1
								}
							};
						Server.newBucket( newBucket )
							.then( function( data, status ){
								getBucket();
								scope.bucketForm = {};
								scope.modalMessage = "Successfully Added Bucket";
								scope.modalTitle = "Success";
								$( "#alertModal" ).modal({
									show: 'show'
								}); 
							} )

						
					}

					scope.addQuote = function(){
						// var addQuote = {
						// 		"feature":
						// 		{
						// 			"link": scope.quoteForm.link,
						// 			"quote": scope.quoteForm.quote,
						// 			"author": scope.quoteForm.author,
						// 			"status": "none"
						// 		}
						// 	};
						// Server.addQuote( '/api/new/feature', addQuote )
						// 	.then( function( data, status ){
						// 		console.log( data, status );
						// 		scope.quoteForm = {};
						// 		scope.modalMessage = "Successfully Added Quote";
						// 		scope.modalTitle = "Success";
						// 		scope.$apply();
						// 		$( "#alertModal" ).modal({
						// 			show: 'show'
						// 		}); 

						// 	} )
						
					}

					scope.addDripAdmin = function(){
						var addDripAdmin = {
								"drip":
								{
									"title": scope.dripForm.title,
									"link": scope.dripForm.link,
									"description": scope.dripForm.post,
									"status":"lock",
									"user_id": "",
									"bucket_id": scope.dripForm.bucket.id,
									"channel": scope.dripForm.channel,
								//	"shares": 1,
								//	"postasname":"",
								//	"postastitle":"",
									"admin_id": scope.admin.id
								}
							};
							Server.getSessionAdmin(  ).then( function( data, status ){
					    		if( data.data.admin ){
					    			
									Server.addDripAdmin( addDripAdmin )
									.then( function( data, status ){
										console.log( data, status );
										updateChannel( data.data.drip.id , scope.dripForm.channel );
										scope.dripForm = {};
										scope.modalMessage = "Successfully Added Drip";
										scope.modalTitle = "Success";
										console.log( "added drip" );
										$( "#alertModal" ).modal({
											show: 'show'
										}); 
									} )

					    		}else{
					    			window.location = "/logout";
					    		}
									
							} )
							
						
							
					};

				}
			}
		}
	] );