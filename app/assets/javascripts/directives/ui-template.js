drip
	.directive( "uiTemplate" , [
		function directive ( ) {
			return {
				"restrict": "E",
				"scope": true,
				"templateUrl": function loadTemplate ( element , attributeSet ) {
					return "/templates/" + attributeSet.url;
				}
			}
		}
	] );