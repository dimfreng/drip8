drip
	.directive( "dashboardDrips" , [
		"Server",
		"Util",
		function directive ( Server , Util ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.drips = [ ];

					scope.getDrips = function ( ) {
						Server.getAllDrips( )
							.then( function ( response ) {
								scope.drips = response.data.drips.reverse( );
							} );
					};

					scope.videoUrl = function ( url ) {						
						var embeddedVideoUrl = "https://www.youtube.com/embed/" + url.split( "v=" )[1] + "?enablejsapi=1";
						return Util.trustUrl( embeddedVideoUrl );
					};
					
					scope.getDrips( );
					console.log( "I have been called" )
				}
			}
		}
	] );