drip
	.directive( "register" , [
		"Server",
		"Util",
		function directive ( Server , Util ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.user = { };

					scope.save = function ( ) {
						scope.user.state = "1";
						scope.user.profile_picture = "nothing";						
						Server.createUser( scope.user )
							.then( function ( response ) {
								console.log( response.data );
								Util.closeModal( );
								scope.user = { };
							} );
					};
				}
			}
		}
	] );