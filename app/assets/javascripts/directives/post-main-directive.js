drip
	.directive( "postMainDirective" , [
		'postData',
		'getData',
		'Server',
		'$rootScope',
		function directive ( postData , getData , Server , $rootScope ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {

					function getSession(){
				    	Server.getSession( ).then( function( data, status ){
							if( data.data.user != null ){
								scope.profileData = data.data.user;

								Server.getBucketByUser( scope.profileData.id ).then( function( data, status ){
									scope.bucket = data.data.bucket;
								} )
							}

						} )
				    }
		            
		            getSession();

					scope.addDrip = function(){
						var newDrip = {
								"drip":{
									"title": scope.mainDrip.title,
									"link": scope.mainDrip.link,
									"description": scope.mainDrip.post,
									"user_id": ( scope.profileData != null ) ? scope.profileData.id : null,
									"bucket_id": ( scope.profileData != null ) ? scope.bucket[0].id : null,
								//	"shares": 1,
									"postasname": ( scope.profileData != null ) ? null : "Anonymous",
									"postastitle": ( scope.profileData != null ) ? null : "Anonymous",
								}
							};
						Server.newDrip( newDrip )
							.then( function( data, status ){
								scope.mainDrip = {
									title: "",
									link: "",
									post: ""
								};

								$rootScope.$broadcast( 'get-top-drips' )
							} )
					}

				}
			}
		}
	] );