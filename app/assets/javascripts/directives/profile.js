drip
	.directive( "profile" , [
		'$rootScope',
		'postData',
		'getData',
		'$window',
		'Server',
		function directive ( $rootScope , postData , getData , $window , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function link( scope , element , attributeSet ){

					scope.social = {
						fb: false,
						twit: false,
						insta: false
					};

					titles = [];
					scope.large = true;
					$window.country = [];
					scope.$watch( function(){
						return $window.country
					}, function( newValue , oldValue ){
						scope.country = newValue;
					} )
		            
				    changeTemplate();
				    function changeTemplate() {

		                var screenWidth = $window.innerWidth;
		                if (screenWidth <= 768) {
		                    scope.large = false;
		                } else if (screenWidth > 768) {
		                    scope.large = true;
		                }
		            }
		            function updateSocial( array ) {

		            	for( var i = 0; i < array.length; i++ ){
		            		Server.updateSocial( {"id":array[i].id, "link": array[i].link} ).then( function( data , status ){} )

		            		if( i ===3 ){
		            			return true
		            		}
		            	}
		                
		            }
				    function getSession(){
				    	Server.getSession(  ).then( function( data, status ){
				    		if( data.data.user != null ){
				    			scope.profile = data.data.user;
								scope.profile.name = scope.profile.firstname + " " + scope.profile.lastname;

								getSocial( scope.profile.id , scope.profile );
								getBucket( scope.profile.id )
								if( scope.profile.usertitle ){
									scope.titles = scope.profile.usertitle.split( "&&" );
								}
								
								if( scope.profile.nationality ){
									scope.nationality = scope.profile.nationality.split( "&&" );
									scope.show = true;
								}

				    		}
								
						} )
				    }
				    function getBucket( id ){
				    	Server.getBucketByUser( id ).success( function( data, status ){
				    		scope.buckets = data.bucket[0];
						} )
				    }
		            
		            getSession();
		            scope.ifProfile = function ifProfile(){
		            	if( scope.profile ){
		            		scope.saveProfile();
		            	}else{
		            		alert( "You need to login" )
		            	}
		            };

		            scope.showCountry = function(){
		            	scope.show = !scope.show;
		            };
		        	
		        	scope.popupBool = false;

		        	scope.showSocial = function showSocial( method ){
		        		switch(method) {
						    case "fb":
						        scope.social = {
									fb: true,
									twit: false,
									insta: false,
									youtube: false
								};
						        break;
						    case "twit":
						        scope.social = {
									fb: false,
									twit: true,
									insta: false,
									youtube: false
								};
						        break;
						    case "insta":
						        scope.social = {
									fb: false,
									twit: false,
									insta: true,
									youtube:false
								};
						        break;
						    case "youtube":
						        scope.social = {
									fb: false,
									twit: false,
									insta: false,
									youtube: true
								};
						        break;
						    default:
						        
						}
		        	};

		        	scope.thumbUrl = function ( url ) {						
							var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
							return embeddedVideoUrl;
					};

					scope.popup = function(){
						if( scope.profile ){
		            		return scope.popupBool = !scope.popupBool;
		            	}else{
		            		alert( "You need to login" )
		            	}
						
					}

					scope.removeTitle = function( index ){
						titles.splice( index , 1 );
						scope.titles = titles;
						scope.profile.usertitle = scope.titles.join( "&&" )
					};
					scope.addTitle = function( usertitle ){

						
					    for (var i = 0 ; i < 3; i++) {
					    	
					    	switch( i ) {
							    case 0:
							        ( titles[ 0 ] == null || titles[ 0 ] == "/^\s*$/" ) ? ( 
							        	titles[ 0 ] = usertitle,
							        	scope.titles = titles,
							        	scope.usertitle = "",
							        	scope.profile.usertitle = scope.titles.join( "&&" ),
								        i = 3 ) : titles[ 0 ];
								        break;
							    case 1:
							        ( titles[ 1 ] == null || titles[ 1 ] == "/^\s*$/" ) ? ( 
							        	titles[ 1 ] = usertitle,
							        	scope.titles = titles,
							        	scope.usertitle = "",
							        	scope.profile.usertitle = scope.titles.join( "&&" ),
							        	i = 3 ) : titles[ 1 ];
							        break;
						        case 2:
							        ( titles[ 2 ] == null || titles[ 2 ] == "/^\s*$/" ) ? ( 
							        	titles[ 2 ] = usertitle,
							        	scope.titles = titles,
							        	scope.usertitle = "",
							        	scope.profile.usertitle = scope.titles.join( "&&" ),
							        	i = 3 ) : titles[ 2 ] ;
							        break;
							    
							}					    
						};
						
					};
					scope.delete = function( id ){
						deleteDrip( id );
					};
					scope.saveProfile = function(){
						var name = scope.profile.name.split( " " );
						var data = {
								"id": scope.profile.id,
								"user":
								{
									"firstname": name[0],
									"lastname": name[1],
									"nationality": scope.country.join( "&&" ),
									"usertitle": scope.profile.usertitle,
									"reminder": false
								}
							};
							
						if( updateSocial( scope.profile.social ) ){
							Server.updateUser( data )
							.then( function ( response ){
								if (response.data.status == 400){
									Server.setSession( response.data.user );
									getSession();
									alert("successfully updated");
									//window.location = "/drips";
								}
							});
						}
					}
					scope.sendToBucket = function(){
						if( scope.profile ){
							window.location = "/bucket";
						}else{
							alert( "You need to login first." )
						}
					}
					function getSocial( id , profile ){
						Server.getSocial( { "id": id } ).then( function( data, status ){
							profile.social = data.data.social;
						} )
					}
					function deleteDrip( id ){
						var answer = confirm ("Delete Drip?");
						if (answer)
						    Server.deleteDrip( {"id":id,"drip":{"user_id":"","bucket_id":"", "description":"","shares":"","channel":"","top":false,"link":"","comments":[],"votes":[]}}).then( function( data, status ){
								getBucket( scope.profile.id );
							} )
						else{}
					}
					scope.openModal = function openModal( drip ){
						
						$rootScope.$broadcast( 'send-drip-to-modal', drip )
					};

					scope.$watch( "popupBool" , function( newValue , oldValue ){
						if( newValue ){
							$(".overlay").css("visibility","visible")
							$(".overlay").css("opacity","1")
							$("#popup2").css("visibility","visible")
							$("#popup2").css("opacity","1")
						}else{
							$(".overlay").css("visibility","hidden")
							$(".overlay").css("opacity","0")
							$("#popup2").css("visibility","hidden")
							$("#popup2").css("opacity","0")
						}
					} )

					
		        }
				
			}
		}
	] );
