drip
	.directive( "videoPlayer" , [
		"$rootScope",
		function directive ( $rootScope ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.controlled = false;
					scope.init = true;
					scope.player = new YT.Player( element[0] );

					scope.player.addEventListener( "onStateChange" , playerStateChange );

					function playerStateChange ( event ) {
						if ( event.data == 1 ) {
							scope.controlled = true;
							$rootScope.$broadcast( "video:pause" );
						}
					}

					scope.$on( "video:pause" , 
						function ( ) {							
							if ( !scope.controlled && !scope.init ) {
								scope.player.pauseVideo( );								
							} else {
								scope.init = false;
							}
							
							scope.controlled = false;
						} );
				}
			}
		}
	] );