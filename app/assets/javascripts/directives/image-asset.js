drip
	.directive( "imageAsset" , [
		"Server",
		function directive ( Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.src = "";

					scope.getSrc = function ( ) {
						Server.asset( attributeSet.imageAsset )
							.then( function ( response ) {
								scope.src = response.data.src;
							} );
					};

					scope.getSrc( );
				}
			}
		}
	] );