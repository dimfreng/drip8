drip
	.directive( "bucketsAdmin" , [
		'Server',
		function directive ( Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"compile": function compile( element , attributeSet ){
		            return{
		                pre: function pre ( scope , element , attributeSet ) {
								
							Server.getUserBucket( {"user_id":"1"} ).then( function( data, status ){
								scope.dataBuckets = data.data.bucket;
								console.log( data );
 								} )
						},
						post: function post( scope, element, attributeSet ){

							console.log( scope.dataBuckets );
						}

		            }
		          }
				
			}
		}
	] );
