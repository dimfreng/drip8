drip
	.directive( "authenticate" , [
		"Server",
		"Util",
		function directive ( Server , Util ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {
					scope.user = { };
					scope.invalid = false;
					scope.message = "";

					scope.login = function ( ) {
						Server.authenticate( scope.user.email , scope.user.password )
							.then( function ( response ) {
								console.log( response.data );
								if ( response.data.value ) {
									Server.setSession( response.data.user )
										.then( function ( ) {
											window.location.reload( );
										} );
								} else {
									var message = response.data.message;
									scope.invalid = true;
									scope.message = message.charAt(0).toUpperCase( ) + message.slice(1);
									if ( response.data.status == 403 )
										scope.user.password = "";
									else if ( response.data.status == 404 )
										scope.user = { };
								}								
							} );
					};

				}
			}
		}
	] );