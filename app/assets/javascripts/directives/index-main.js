drip
	.directive( "indexMainDirective" , [
		'Util',
		'Video',
		'quotesFactory',
		function directive ( Util , Video , quotesFactory ) {
			return {
				"restrict": "A",
				"scope": true,
				"compile": function onCompile ( element , attributeSet ) {

					return {
						pre: function pre( scope , element , attributeSet ){
							scope.quotes = quotesFactory;


							scope.videoUrl = function ( url ) {						
								var embeddedVideoUrl = "https://www.youtube.com/embed/" + url.split( "v=" )[1] + "?enablejsapi=1";
								return Util.trustUrl( embeddedVideoUrl );
							};
							scope.thumbUrl = function ( url ) {						
								var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
								//console.log( embeddedVideoUrl )
								return embeddedVideoUrl;
							};
							scope.videoPlay = scope.quotes[ scope.quotes.length - 1 ].link; //Video.videoSource( scope.quotesArray[ scope.quotesArray.length - 1 ].link.split( "v=" )[1] );
							scope.quoteActive = scope.quotes[ scope.quotes.length - 1 ].quote;
							scope.authorActive = scope.quotes[ scope.quotes.length - 1 ].author;

							$(document).ready(function() {
								$("#owl-demo").owlCarousel({
							        /* autoPlay: 3000, */
							        items : 4,
							        itemsDesktop : [1199,3],
							        itemsDesktopSmall : [979,3],
							        navigation: true,
							        pagination: false
							      });
    						});
							

							
							scope.linkUri = function linkUri ( link , service ) {
								var video_id = link.split( 'v=' )[1];
								var ampersandPosition = video_id.indexOf( '&' );
								console.log( ampersandPosition )
								if ( ampersandPosition != -1 ) {
								  video_id = video_id.substring( 0 , ampersandPosition );
								}
								if ( service == "thumb" ) {
									return Video.thumbnail( video_id );
								} else {
									return Video.videoSource( video_id );
								}	
							};
						},

						post: function post( scope , element , attributeSet ){
							scope.login = function login(){
								window.location = "/auth/facebook";
							};
							scope.see = function see(){
								window.location = "/drips";
							}
							scope.changeVideo = function changeVideo( link , quote , author ){
								scope.videoPlay = link;
								scope.quoteActive = quote;
								scope.authorActive = author;
							};
							
						}
					}

				}
			}
		}
	] );
/*scope.$on( 'onDataQuotes' , function( evt , data ){
								
	for (i = 0; i < data.length; i++) { 
	    scope.quotes.push( scope.thumbUrl( data[i].link ) );

	}
	quoteService.getdata( scope.quotes );
	
	
} )

	
getData.getdata( '/api/get/feature' ).success( function( data, status ){
	scope.quotesArray = data.features;
	
	scope.$broadcast( 'onDataQuotes', scope.quotesArray );

	
} )*/