drip
	.directive( "dripsAdminDirective" , [
		'Server',
		'postData',
		'Util',
		function directive ( Server , postData ,  Util ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {

					scope.videoUrl = function ( url ) {						
						var embeddedVideoUrl = "https://www.youtube.com/embed/" + url.split( "v=" )[1] + "?enablejsapi=1";
						return Util.trustUrl( embeddedVideoUrl );
					};
					scope.thumbUrl = function ( url ) {						
						var embeddedVideoUrl = "https://img.youtube.com/vi/" + url.split( "v=" )[1] + "/0.jpg";
						//console.log( embeddedVideoUrl )
						return embeddedVideoUrl;
					};
					getSession();
					function getSession(){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.admin ){
				    			scope.admin = data.data.admin;
				    		}else{
				    			window.location = "/logout";
				    		}
								
						} )
					}
					
					scope.getWhat = function getWhat( method ){
						scope.method = method;
						Server.getWhat( method ).then( function( data, status ){
							scope.dripsArray = data.data.drips;
							//scope.$apply();
						} )
					};
					
					scope.addToTop = function addToTop( drip ){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.admin ){
				    			

								Server.addToTopDrip( {"id": drip.id, "top":true} )
									.then( function( data, status ){
										scope.getWhat( scope.method );
									} )

				    		}else{
				    			window.location = "/logout";
				    		}
								
						} )
				    		
					};

					scope.dropFromTop = function dropFromTop( drip ){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.admin ){
				    			Server.dropFromTopDrip( {"id": drip.id,"drip":{"top":false}} )
								.then( function( dataDropFromTopDrip, status ){
									scope.getWhat( scope.method );
								} )
				    		}else{
				    			window.location = "/drip8-adminshackle/logout";
				    		}
								
						} )
				    		
						
					}
					

				}
			}
		}
	] );