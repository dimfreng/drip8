var drip = angular.module( "drip8" , [ 'templates' ] );

drip
	.config( [
		"$httpProvider",
		function configurations ( $httpProvider ) {
			$httpProvider.defaults.headers.common[ "X-CSRF-Token" ] = $( "meta[name=csrf-token]" ).attr( "content" );

		}
	] );

drip
	.run( [
		'$rootScope',
		'$location',
		function( $rootScope, $location ) {

			function nth_ocurrence(str, needle, nth) {
			  for (i=0;i<str.length;i++) {
			    if (str.charAt(i) == needle) {
			        if (!--nth) {
			           return i;    
			        }
			    }
			  }
			  return false;
			}
			var location = $location.$$absUrl;
			var position = nth_ocurrence( location, "/" , 4 );
			var path = location.slice( position + 1 , location.last );
			
			$rootScope.locationToPath = path;
			
		}
	] );
drip
	.factory( "Video" , [
		"$sce",
		function factory ( $sce ) {
			this.thumbnail = function thumbnail ( youtubeId ) {
				return $sce.trustAsResourceUrl( "http://img.youtube.com/vi/" + youtubeId + "/0.jpg" );
			};

			this.videoSource = function videoSource ( youtubeId ) {
				return $sce.trustAsResourceUrl( "http://www.youtube.com/embed/" + youtubeId + "?rel=0&nologo=1&showinfo=0&modestbranding=1&iv_load_policy=3" );
			};

			return this;
		}
	] );
drip.service('postData', 
	[
		'$http',
		function service( $http ){

			return {
				postdata: function( host, data ){
					
						var promise = $.post(  host, data )
						.success( function(data){
							
						} )
						.error( function(error){
						} );
						
					return promise;
				}
			}
		}
	]);
drip.service('getData', 
	[
		'$http',
		function factory( $http ){

			return {
				getdata: function ( host ){
					var promise = $http.get( host )
									.success(function( getData ){
										var data = getData;
										return data;
									});
					return promise;
				} 
			}
			
		}
	]);
drip.service('quoteService', 
	[
		'$rootScope',
		function factory( $rootScope ){
			var quotes = "";
			return {
				getdata: function ( condition ){
					if( condition ){
						quotes = condition;

						$rootScope.$broadcast( "loadedQuote" )
					}else{

						return quotes;
					}
				} 
			}
			
		}
	]);

drip
	.directive( "carouselDirective" , [
		'$rootScope',
		'quoteService',
		function directive ( $rootScope , quoteService ) {
			return {
				"restrict": "A",
				"scope": {},
				"link": function onLink ( scope , element , attributeSet ) {

					$rootScope.$on( 'loadedQuote' , function( evt , data ){
						scope.quotes = quoteService.getdata();

					} )
					
					
				}
			}
		}
	] );
drip
	.directive( "quoteDirective" , [
		'$rootScope',
		'getData',
		'postData',
		'Server',
		function directive ( $rootScope , getData , postData , Server ) {
			return {
				"restrict": "A",
				"scope": true,
				"link": function onLink ( scope , element , attributeSet ) {

					getData.getdata( '/api/get/feature' ).success( function( data, status ){
						scope.quotesArray = data.features;

						//scope.$apply();
					} )
					
					scope.editQuote = function( quote ){

						scope.chosenToEdit = quote;
					};
					getSession();
					function getSession(){
						Server.getSessionAdmin(  ).then( function( data, status ){
				    		if( data.data.user === true ){
				    		}else
				    			window.location = "/logout";
								
						} )
					}
					scope.submitEditQuote = function( ){
						postData.postdata( '/api/update/feature', 
						{
							"id": scope.chosenToEdit.id, 
							"feature": {
								"link": scope.chosenToEdit.link,
								"quote": scope.chosenToEdit.quote,
								"author": scope.chosenToEdit.author,
							}
						} ).success( function( data, status ){
							
							scope.chosenToEdit = {};
							$( "#editModal" ).modal({
								show: false
							}); 

							scope.$apply();
						} )
					}
				}
			}
		}
	] );





