class AdminController < ApplicationController
	before_filter :check_no_session, only: [ "drips" , "quotes" , "users" , "privilege" ]

	def drips
		render layout: "admin"
	end

	def login
		render layout: "admin-login"
	end

	def privilege
		render layout: "admin"
	end
	
	def quotes
		render layout: "admin"
	end

	def users
		render layout: "admin"
	end

	def get_session_admin
		render json: { admin: session[ :admin ] }
	end
	def logout
		session[ :admin ] = nil
		redirect_to :action => "login"
	end

	protected

	def check_session
		unless !session[ :admin ]
			redirect_to :action => "dashboard"
		end

		return true
	end

	def check_no_session
		unless session[ :admin ]
			redirect_to :action => "login"
		end

		return true
	end
end
