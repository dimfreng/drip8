class PageController < ApplicationController

	before_filter :check_no_session, only: [ "profile" , "get_session" ]
	before_filter :check_session, only: [ "index" ]



	def facebook_callback
		@oauth = request.env['omniauth.auth']

		if @oauth
			@user = User.find_by_email( @oauth[:info][:email] )

			if @user == nil
				name = @oauth[:info][:name].strip.split(/\s+/)
				@user = User.create( email: @oauth[:info][:email] , firstname: name.first, lastname: name.last , profile_picture: @oauth[:info][:image], password: "12345" )
				@bucket = Bucket.create(:name => "Drips", :user_id => @user.id)
				$i = 0
				$num = 4
				begin
				   @media = Socialmedium.create(:user_id => @user.id , :link => "")
				   $i +=1
				end while $i < $num
				
			end
			session[:user] = @user
			session[:oauth] = request.env['omniauth.auth']			
			redirect_to action: 'dashboard'
		else
			redirect_to action: 'index'
		end
	end

	def index
		
	end

	def dashboard
		@user = session[:user]
		render layout: "drip"
	end

	def profile
		render layout: "drip"
	end

	def set_session
		session[ :user ] = nil
		session[ :user ] = params[ :user ]
		render json: { session: session[:user] , status: "true" }
	end

	def get_session
		render json: { user: session[ :user ] }
	end
	

	 def routing_error
	    redirect_to action: "index"
	  end

	def logout
		session[ :user ] = nil
		redirect_to :action => "index"
	end

	def image_asset
		render json: { src: view_context.image_path( params[ :src ] ) }
	end

	protected

	def check_session
		unless !session[ :user ]
			redirect_to :action => "dashboard"
		end

		return true
	end

	def check_no_session
		unless session[ :user ]
			redirect_to :action => "index"
		end

		return true
	end

end
