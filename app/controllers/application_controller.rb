class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :set_access_control_headers, :cors_preflight
  after_filter :set_csrf_cookie_for_ng   

  def cors_preflight
    if request.method == "OPTIONS"      
      respond_to do |format|
        format.json { render json:{ :status => "success" } , status: :ok }
      end
    end
  end

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = 'https://drip8-v2.herokuapp.com , www.drip8.com'
    headers['Access-Control-Allow-Methods'] = 'POST,GET,OPTION'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Content-Type'
  end

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

 

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
end
