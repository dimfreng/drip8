class ApiController < ApplicationController

# user

	def create_user
		if User.find_by_email(params[:user][:email])
			status = 404
			message = "email already exist."
		else
			if user = User.create(user_params)
				message = "succesfully created."
				status = 400
			else
				status = 404
				message = "Failed to create User."
			end
		end
		
		render json: {user: user, message: message, status: status}
	end

	

	# def set_user_session_by_id
	# 	session[:user] = nil
	# 	if user = User.get_user(params[:id])
	# 		message = "succesfully retrieve."
	# 		status = 400
	# 		session[:user] = user
	# 	else
	# 		status = 404
	# 		message = "User not found."
	# 	end
	# 	render json:{session: session[:user]}
	# end

	def get_user_by_id
		if user = User.get_user(params[:id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {user: user, message: message, status: status}
	end

	def get_all_users
		if users = User.get_all_user
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {users: users, message: message, status: status}
	end



	def authenticate_user
		if user = User.find_by_email(params[:email])
			if user.authenticate(params[:password])
				value = true
				message = "succesfully login"
				status = 400
			else	
				user = nil
				value = false
				message = "wrong password"
				status = 403
			end
		else	
			value = false
			message = "email not found"
			status = 404
		end

		render json: {user: user.as_json(except: :password_digest), value: value, message: message, status: status}
		
	end


	# def get_user_session
	# 	render json:{session: session[:user]}
	# end


	def update_user_details
		if user = User.find_by_id(params[:id])
			if user.update_attributes(user_params)
				status = 400
				message = "succesfully update"
				session[:user] = nil
				session[:user] = user

			else
				user = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "User not found."
		end

		render json: {user: user.as_json(except: :password_digest), message: message, status: status}
	end
	def get_media_by_user_id
		if social = Socialmedium.get_media_by_user_id(params[:id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {social: social}
	end
	def update_media_by_id
		if media = Socialmedium.find_by_id(params[:id])
			if media.update_attributes(:link => params[:link])
				status = 400
				message = "succesfully update"
			else
				drip = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "Socialmedia not found."
		end
		render json: {media: media, message: message, status: status}
	end

# drips
	
	def create_drip
		if drip = Drip.create(drip_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create drip."
		end
		render json: {drip: drip, message: message, status: status}
	end

	def get_drip_by_id
		if drip = Drip.get_drip(params[:id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "drip not found."
		end
		render json: {drip: drip, message: message, status: status}
	end

	def get_all_drip_of_admin
		if drips = Drip.get_all_drip_of_admin
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {drips: drips, message: message, status: status}
	end

	def get_all_drip_of_users
		if drips = Drip.get_all_drip_of_users
			Drip.order('id')
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {drips: drips, message: message, status: status}
	end

	def get_drips_by_user
		if drip = Drip.get_drips_by_user(params[:user_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "drip not found."
		end
		render json: {drip: drip, message: message, status: status}
	end

	def get_all_drip_of_anon
		if drip = Drip.get_all_drip_of_anon
			message = "succesfully get"
		else
			message = "error"
		end

		render json:{drips: drip, message: message}
	end

	def get_top_drips
		if drip = Drip.get_all_top_drips
			message = "succesfully get"
		else
			message = "error"
		end

		render json:{drips: drip, message: message}
	end

	def update_drip_details
		if drip = Drip.find_by_id(params[:id])
			if drip.update_attributes(drip_params)
				status = 400
				message = "succesfully update"
			else
				drip = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "drip not found."
		end
		render json: {drip: drip.as_json(except: :password_digest), message: message, status: status}
	end

	def update_drip_channel
		if drip = Drip.find_by_id(params[:id])
			if drip.update_attributes(:channel => params[:channel])
				status = 400
				message = "succesfully update"
			else
				drip = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "drip not found."
		end
		render json: {drip: drip.as_json(except: :password_digest), message: message, status: status}
	end

	def update_drip_top
		if drip = Drip.find_by_id(params[:id])
			if drip && drip.update_attributes(:top => params[:top])
				status = 400
				message = "succesfully update"
			else
				drip = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "drip not found."
		end
		render json: {drip: drip.as_json(except: :password_digest), message: message, status: status}
	end

# buckets

	def create_bucket
		if bucket = Bucket.create(bucket_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create bucket."
		end
		render json: {bucket: bucket, message: message, status: status}
	end

	def get_bucket_by_id
		if bucket = Bucket.get_bucket(params[:id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "bucket not found."
		end
		render json: {bucket: bucket, message: message, status: status}
	end

	def get_all_buckets
		if buckets = Bucket.get_all_bucket
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {buckets: buckets, message: message, status: status}
	end

	def get_buckets_by_user
		if bucket = Bucket.get_buckets_by_user(params[:user_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "bucket not found."
		end
		render json: {bucket: bucket, message: message, status: status}
	end

	def update_bucket_details
		if bucket = Bucket.find_by_id(params[:id])
			if bucket.update_attributes(bucket_params)
				status = 400
				message = "succesfully update"
			else
				bucket = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "bucket not found."
		end
		render json: {bucket: bucket.as_json(except: :password_digest), message: message, status: status}
	end

# comment

	def create_comment
		if comment = Comment.create(comment_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create comment."
		end
		render json: {comment: comment, message: message, status: status}
	end
	
	def get_comment_by_id
		if comment = Comment.get_comment(params[:id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "comment not found."
		end
		render json: {comment: comment, message: message, status: status}
	end

	def get_comment_by_user
		if comment = Comment.get_comments_by_user(params[:user_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "comment not found."
		end
		render json: {comment: comment, message: message, status: status}
	end

	def get_all_comment_by_drip
		if comments = Comment.get_all_comment_by_drip(params[:drip_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {comments: comments, message: message, status: status}
	end

	def update_comment
		if comment = Comment.find_by_id(params[:id])
			if comment.update_attributes(comment_params)
				status = 400
				message = "succesfully update"
			else
				comment = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "comment not found."
		end
		render json: {comment: comment.as_json(except: :password_digest), message: message, status: status}
	end

# admin
	
	def create_admin
		if admin = Admin.create(admin_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create admin."
		end
		render json: {admin: admin, message: message, status: status}
	end


	def authenticate_admin
		if admin = Admin.find_by_username(params[:username])
			if admin.authenticate(params[:password])
				value = true
				message = "succesfully login"
				status = 400
				session[:admin] = admin
			else	
				admin = nil
				value = false
				message = "wrong password"
				value = 404
				session[:admin] = false
			end
		else	
			value = false
			message = "username not found"
			status = 404
		end

		render json: {admin: admin.as_json(except: :password_digest), value: value, message: message, status: status}
	end

	def change_password
		if admin = Admin.find_by_id(params[:id])
			if admin.update_attributes(admin_params)
				status = 400
				message = "succesfully update"
			else
				admin = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "admin not found."
		end
		render json: {admin: admin.as_json(except: :password_digest), message: message, status: status}
	end

#feature

	def create_feature
		if feature = Feature.create(feature_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create feature."
		end
		render json: {feature: feature, message: message, status: status}
	end


	def get_features
		if feature = Feature.all
			message = "succesfully processed"
		else
			message = "error"
		end

		render json: {features: feature, message: message}
	end

	def update_feature
		if feature = Feature.find_by_id(params[:id])
			if feature.update_attributes(feature_params)
				status = 400
				message = "succesfully update"
			else
				feature = nil
				message = "failed to udpate"
				status = 404
			end
		else
			status = 404
			message = "feature not found."
		end
		render json: {feature: feature}
	end

#vote
	
	def create_vote
		if vote = Vote.create(vote_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create vote."
		end
		render json: {vote: vote, message: message, status: status}	
	end	

	def get_votes_by_drip
		if votes = Vote.get_all_vote_by_drip(params[:drip_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {votes: votes, vote_count: votes.count, message: message, status: status}
	end

	def get_votes_by_user
		if vote = Vote.get_votes_by_user(params[:user_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "vote not found."
		end
		render json: {vote: vote, message: message, status: status}
	end

#socialmedia
	
	def create_media
		if media = Socialmedium.create(socialmedia_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create vote."
		end
		render json: {media: media, message: message, status: status}	
	end	

	def get_media_by_user
		if media = Socialmedium.find_by_id(params[:user_id])
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "User not found."
		end
		render json: {media: media, message: message, status: status}
	end

#reports
	
	def create_report
		if report = Report.create(report_params)
			message = "succesfully created."
			status = 400
		else
			status = 404
			message = "Failed to create report."
		end
		render json: {report: report, message: message, status: status}	
	end

	def get_all_reports
		if report = Report.get_all_reports
			message = "succesfully retrieve."
			status = 400
		else
			status = 404
			message = "No reports."
		end
		render json: {report: report, message: message, status: status}	
	end


	private

	def report_params
		params.require(:report).permit(:drip_id, :body)
	end

	def user_params
		params.require(:user).permit(:firstname, :lastname, :email, :profile_picture, :state, :password, :usertitle, :number , :nationality, :reminder)
	end

	def drip_params
		params.require(:drip).permit(:title, :description, :link, :status, :user_id, :bucket_id, :shares, :postasname, :postastitle, :admin_id)
	end

	def bucket_params
		params.require(:bucket).permit(:name, :status, :user_id)
	end

	def comment_params
		params.require(:comment).permit(:content, :user_id, :drip_id)
	end

	def admin_params
		params.require(:admin).permit(:username, :password)
	end

	def feature_params
		params.require(:feature).permit(:link, :quote, :author, :status)
	end

	def vote_params
		params.require(:vote).permit(:user_id, :drip_id)
	end

	def socialmedia_params
		params.require(:social).permit(:user_id, :link)
	end


end
