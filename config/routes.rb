Rails.application.routes.draw do


 

  post '/api/get/user'           => 'api#get_user_by_id'          #{"id":"1"}
  get  '/api/get/all/user'       => 'api#get_all_users'           #{}
  post '/api/user/authenticate'  => 'api#authenticate_user'       #{"email":"benantolisdimful@gmail.com","password":"123"}
  post '/api/new/user'           => 'api#create_user'             #{"user":{"firstname":"Geoff","lastname":"Diaz","email":"diazgeoff93@gmail.com","profile_picture":"nothing","state":"1","password":"123", "usertitle":"pusher","number":"0000","reminder":"true", "nationality":"filipino"}}
  post '/api/user/update'        => 'api#update_user_details'     #{"id":"3","user":{"firstname":"Geoffy"}}
  post '/api/get/drip'           => 'api#get_drip_by_id'          #{"id":"1"}
  get  '/api/get/all/drip/admin' => 'api#get_all_drip_of_admin'   #{}
  get  '/api/get/all/drip/user'  => 'api#get_all_drip_of_users'   #{}
  post '/api/get/user/drip'      => 'api#get_drips_by_user'       #{"user_id":"1"}
  post '/api/update/drip'        => 'api#update_drip_details'     #{"id":"1","drip":{"status":"ok"}}
  post '/api/new/drip'           => 'api#create_drip'             #{"drip":{"title":"teenager","link":"youtube","description":"first drip","status":"lock","user_id":"1","bucket_id":"1","shares":"1","postasname":"anonymous","postastitle":"anonymous pusher","admin_id":"if si admin ang mag buhat og drips"}}
  post '/api/get/bucket'         => 'api#get_bucket_by_id'        #{"id":"1"}
  get  '/api/get/all/bucket'     => 'api#get_all_buckets'         #{}     
  post '/api/get/user/bucket'    => 'api#get_buckets_by_user'     #{"user_id":"1"}
  post '/api/update/bucket'      => 'api#update_bucket_details'   #{"id":"1","bucket":{"name":"okok"}}
  post '/api/new/bucket'         => 'api#create_bucket'           #{"bucket":{"name":"whoah","status":"ok","user_id":"1"}} 
  post '/api/get/comment'        => 'api#get_comment_by_id'       #{"id":"1"}
  post '/api/get/user/comment'   => 'api#get_comment_by_user'     #{"user_id":"1"}
  post '/api/get/drip/comment'   => 'api#get_all_comment_by_drip' #{"drip_id":"2"}
  post '/api/update/comment'     => 'api#update_comment'          #{"id":"1","comment":{"content":"nice try"}}
  post '/api/new/comment'        => 'api#create_comment'          #{"comment":{"content":"nice drip", "user_id":"1","drip_id":"1"}}
  post '/api/admin/authenticate' => 'api#authenticate_admin'      #{"username":"dimfusl","password":"pangit"}
  post '/api/new/admin'          => 'api#create_admin'            #{"admin":{"username":"dimful","password":"gwapo"}}
  post '/api/update/admin'       => 'api#change_password'         #{"id":"1","admin":{"password":"hello"}}
  post '/api/new/feature'        => 'api#create_feature'          #{"feature":{"link":"this link","quote":"duterte","author":"duterte","status":"angry"}}
  post '/api/new/vote'           => 'api#create_vote'             #{"vote":{"user_id":"1","drip_id":"1"}}
  post '/api/get/drip/vote'      => 'api#get_votes_by_drip'       #{"drip_id":"1"}
  post '/api/get/user/votes'     => 'api#get_votes_by_user'       #{"user_id":"1"}
  get  '/api/get/feature'        => 'api#get_features'            #{}
  post '/api/update/feature'     => 'api#update_feature'          #{"id":"1", "feature":{"link":"this link"}}
  get  '/api/get/top'            => 'api#get_top_drips'           #{}
  post '/api/update/drip/channel'=> 'api#update_drip_channel'     #{"id":"1","channel":""}
  post '/api/update/drip/top'    => 'api#update_drip_top'         #{"id":"1","top":""}
  get  '/api/get/drips/anon'     => 'api#get_all_drip_of_anon'    #{}
  post '/api/set/session'        => 'api#set_user_session_by_id'  #{"id": "1"}
  post '/api/update/social'      => 'api#update_media_by_id'      #{"id":"1","link":""}
  post '/api/get/social'         => 'api#get_media_by_user_id'    #{"id":"1"}
  #Page Routes



  get  '/'            => 'page#index'
  get  '/drips'        => 'page#dashboard'
  get  '/bucket'     => 'page#profile'
  get  '/logout'      => 'page#logout'
  post '/auth'        => 'page#auth'
  post '/session/get'  => 'page#get_session'
  post '/session/set' => 'page#set_session'
  post '/asset/img'   => 'page#image_asset'

#facebook api
  get '/auth/facebook/callback' => 'page#facebook_callback'
  #admin Routes

  get '/drip8-adminshackle/drips'       => 'admin#drips'
  get '/drip8-adminshackle/login'       => 'admin#login'
  get '/drip8-adminshackle/privilege'   => 'admin#privilege'
  get '/drip8-adminshackle/quotes'      => 'admin#quotes'
  get '/drip8-adminshackle/users'       => 'admin#users'
  get '/drip8-adminshackle/logout'      => 'admin#logout'
  post '/api/new/media'                 => 'api#create_media'               #{"social":{"user_id":"1","link":"http:dsadasd"}}
  post '/api/get/media'                 => 'api#get_media_by_user' 
  get  '/admin/session'                 => 'admin#get_session_admin'        #{}
   match "*a", :to => "page#routing_error", :via => [:get, :post, :options]  
  post '/api/new/media'          => 'api#create_media'            #{"social":{"user_id":"1","link":"http:dsadasd"}}
  post '/api/new/report'         => 'api#create_report'           #{"report":{"drip_id":"1","body":"blablabla"}}
  get  '/api/get/reports'        => 'api#get_all_reports'         #{}
  post '/api/get/media'          => 'api#get_media_by_user'       #{"user_id":"1"}
end
